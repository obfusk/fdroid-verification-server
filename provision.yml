---
- hosts: all
  remote_user: root

  vars:
    user: "fdroid"
    userhome: "/home/{{user}}"
    fdroidserver_dir: "{{userhome}}/fdroidserver"

  tasks:
    - name: "set hostname"
      hostname:
        name: verification.f-droid.org
    - name: "authorized_keys: set up the standard admins"
      authorized_key:
        user: root
        key: "{{ item.key }}"
        state: present
        unique: yes
      with_list: "{{ authorized_keys }}"

    - name: "apt: install debian packages for apt setup"
      apt:
        name: "{{item}}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:
        - apt-transport-https
        - apt-transport-tor
        - debian-archive-keyring
        - gnupg

    - name: "copy: script for updating with apt"
      copy:
        mode: 0700
        content: |
          #!/bin/sh

          set -x
          apt-get update
          apt-get -y dist-upgrade --download-only

          set -e
          apt-get -y upgrade
          apt-get dist-upgrade
          apt-get autoremove --purge
          apt-get clean
        dest: /root/update-all

    - name: "apt_repository: enable stretch-backports"
      apt_repository:
        repo: 'deb http://deb.debian.org/debian stretch-backports main'
        state: present
        update_cache: no
    - name: "copy: apt pinning rule for backports packages"
      copy:
        content: |
          Package: androguard diffoscope fdroidserver python3-asn1crypto python3-mwclient vagrant
          Pin: release a=stretch-backports
          Pin-Priority: 500
        dest: /etc/apt/preferences.d/debian-stretch-backports.pref

    - name: "apt: update and upgrade"
      apt:
        update_cache: yes
        upgrade: dist

    - name: "apt: install debian packages"
      apt:
        name: "{{item}}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:

        # useful utils
        - bash-completion
        - curl
        - emacs-nox
        - figlet
        - htop
        - iotop
        - less
        - ncdu
        - unattended-upgrades
        - vim
        - wget

        # verification server
        - android-sdk
        - fdroidserver
        - nfs-kernel-server
        - rsync
        - screen

    - name: "apt: install diffoscope with recommends"
      apt:
        name: "{{item}}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: yes
      with_items:
        - diffoscope

    - locale_gen:
        name: de_AT.UTF-8
        state: present
    - locale_gen:
        name: de_DE.UTF-8
        state: present
    - locale_gen:
        name: en_US.UTF-8
        state: present
    - locale_gen:
        name: en_GB.UTF-8
        state: present
    - name: 'lineinfile: set locale to en_US.UTF-8'
      lineinfile:
        dest: "/etc/default/locale"
        line: "LANG=en_US.UTF-8"

    - name: "shell: set motd"
      shell: |
        echo > /etc/motd
        figlet verification >> /etc/motd
        printf '\ncreated with https://gitlab.com/fdroid/fdroid-verification-server\n\n' >> /etc/motd

    - name: "git: clone fdroidserver"
      git:
        repo: 'https://gitlab.com/fdroid/fdroidserver.git'
        dest: '{{fdroidserver_dir}}'
        version: master
        force: yes
      become: yes
      become_user: "{{user}}"

    - name: "copy: create {{fdroidserver_dir}}/makebuildserver.config.py"
      copy:
        mode: 0600
        owner: "{{user}}"
        content: |
          basebox = "fdroid/basebox-stretch64"
          apt_package_cache = True
          debian_mirror = 'http://deb.debian.org/debian/'
          memory = 6144
          cpus = 4
          vm_provider = 'libvirt'
          libvirt_disk_bus = "sata"
          libvirt_nic_model_type = "rtl8139"
        dest: '{{fdroidserver_dir}}/makebuildserver.config.py'

    - name: "copy: grant rights to {{user}} for changing access permissions to vm image"
      copy:
        content: |
          Cmnd_Alias VAGRANT_EXPORTS_CHOWN = /bin/chown 0\:0 /tmp/*
          Cmnd_Alias VAGRANT_EXPORTS_MV = /bin/mv -f /tmp/* /etc/exports
          Cmnd_Alias VAGRANT_NFSD_CHECK = /bin/systemctl status --no-pager nfs-server.service
          Cmnd_Alias VAGRANT_NFSD_START = /bin/systemctl start nfs-server.service
          Cmnd_Alias VAGRANT_NFSD_APPLY = /usr/sbin/exportfs -ar
          %{{user}} ALL=(root) NOPASSWD: VAGRANT_EXPORTS_CHOWN, VAGRANT_EXPORTS_MV, VAGRANT_NFSD_CHECK, VAGRANT_NFSD_START, VAGRANT_NFSD_APPLY
        dest: "/etc/sudoers.d/vagrant-nfs-synced-folder"
      become: yes

    - name: "replace: 9p synced_folders with nfs"
      replace:
        destfile: "{{fdroidserver_dir}}/buildserver/Vagrantfile"
        regexp: "'9p'"
        replace: "'nfs'"
      become: yes
      become_user: "{{user}}"

    - name: "git: clone fdroiddata"
      git:
        repo: 'https://gitlab.com/fdroid/fdroiddata.git'
        dest: '{{userhome}}/fdroiddata'
        version: master
        force: yes
      become: yes
      become_user: "{{user}}"

    - name: "file: allow {{user}} to write in webroot"
      file:
        path: /var/www/html
        owner: "{{user}}"
        group: "{{user}}"
        mode: 0755

    - name: "file: symlink unsigned/ to become the webroot"
      file:
        src: /var/www/html
        dest: '{{userhome}}/fdroiddata/unsigned'
        owner: "{{user}}"
        group: "{{user}}"
        state: link
        force: yes

    - name: "file: symlink unsigned/ to be available in the webroot"
      file:
        src: /var/www/html
        dest: /var/www/html/unsigned
        state: link
        force: yes

    - name: "file: symlink tmp/ to be available in the webroot"
      file:
        src: '{{userhome}}/fdroiddata/tmp'
        dest: /var/www/html/tmp
        owner: "{{user}}"
        group: "{{user}}"
        state: link
        force: yes

    - name: "copy: Apache index header into webroot"
      copy:
        mode: 0644
        src: HEADER.html
        dest: /var/www/html

    - name: "copy: script to calculate which builds to run"
      copy:
        mode: 0755
        owner: "{{user}}"
        src: get-recent-apks-list.py
        dest: '{{ userhome }}/get-recent-apks-list.py'

    - name: "copy: script to run builds"
      copy:
        mode: 0755
        owner: "{{user}}"
        src: run-mass-build
        dest: '{{ userhome }}/run-mass-build'

    - name: "file: create log dir"
      file:
        path: /var/log/fdroid
        state: directory
        owner: root
        group: "{{user}}"
        mode: 0775

    - name: "copy: cron job to run builds daily"
      copy:
        mode: 0755
        content: |
          #!/bin/sh -e
          jobname=run-mass-build
          su --login {{user}} --command "/bin/sh -e" <<EOF
              if screen -list | grep --fixed-strings "${jobname}"; then
                  exit 0
              fi
              screen -d -m -L /var/log/fdroid/${jobname}-`date '+%s'`.log \
                  -S ${jobname} \
                  -t ${jobname} \
                  /home/{{user}}/${jobname}
          EOF
        dest: /etc/cron.daily/run-mass-build

    - name: "copy: script to run verification"
      copy:
        mode: 0755
        owner: "{{user}}"
        src: run-mass-verify
        dest: '{{ userhome }}/run-mass-verify'

    - name: "copy: cron job to run verification daily"
      copy:
        mode: 0755
        content: |
          #!/bin/sh -e
          jobname=run-mass-verify
          su --login {{user}} --command "/bin/sh -e" <<EOF
              if screen -list | grep --fixed-strings "${jobname}"; then
                  exit 0
              fi
              screen -d -m -L /var/log/fdroid/${jobname}-`date '+%s'`.log \
                  -S ${jobname} \
                  -t ${jobname} \
                  /home/{{user}}/${jobname}
          EOF
        dest: /etc/cron.daily/run-mass-verify
