#!/usr/bin/env python3

from datetime import datetime

apklist = []
with open('stats/known_apks.txt', 'r', encoding='utf8') as fp:
    for line in fp:
        t = line.rstrip().split(' ')
        if len(t) > 2: # old format didn't have the date
            #print(line)
            appid = t[-2]
            date = datetime.strptime(t[-1], '%Y-%m-%d')
            filename = line[0:line.rfind(appid) - 1]
            apklist.append([date, appid])

# manually add apps to keep here            
appids = [
    'org.briarproject.briar.android',
    'org.fdroid.fdroid',
    'org.fdroid.fdroid.privileged',
    'org.fdroid.fdroid.privileged.ota',
    'org.smssecure.smssecure',
    'eu.siacs.conversations',
    'org.telegram.messenger',
    'org.gnu.icecat',
    'org.mozilla.klar',
    'org.sufficientlysecure.keychain',
]
ignore = [
    'org.tasks',
]
deleteme = []

for app in sorted(apklist, reverse=True):
    appid = app[1].strip()
    if len(appids) < 1500:
        if appid not in appids:
            appids.append(appid)
    else:
        if appid not in deleteme:
            deleteme.append(appid)

with open('build.txt', 'w', encoding='utf8') as fp:
    for appid in appids:
        if appid not in ignore:
            fp.write(appid + '\n')

with open('delete.txt', 'w', encoding='utf8') as fp:
    for appid in deleteme:
        if appid not in ignore:
            fp.write(appid + '\n')
